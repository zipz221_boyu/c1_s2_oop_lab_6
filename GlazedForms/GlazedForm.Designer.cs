﻿namespace GlazedForms
{
    partial class GlazedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sizeGroupBox = new System.Windows.Forms.GroupBox();
            this.heightNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.heightLabel = new System.Windows.Forms.Label();
            this.widthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.widthLabel = new System.Windows.Forms.Label();
            this.materialLabel = new System.Windows.Forms.Label();
            this.materialСomboBox = new System.Windows.Forms.ComboBox();
            this.glazingGroupBox = new System.Windows.Forms.GroupBox();
            this.doubleGlazingRadioButton = new System.Windows.Forms.RadioButton();
            this.singleGlazingRadioButton = new System.Windows.Forms.RadioButton();
            this.windowsillCheckBox = new System.Windows.Forms.CheckBox();
            this.resultLabel = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.sizeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heightNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthNumericUpDown)).BeginInit();
            this.glazingGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // sizeGroupBox
            // 
            this.sizeGroupBox.AutoSize = true;
            this.sizeGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.sizeGroupBox.Controls.Add(this.heightNumericUpDown);
            this.sizeGroupBox.Controls.Add(this.heightLabel);
            this.sizeGroupBox.Controls.Add(this.widthNumericUpDown);
            this.sizeGroupBox.Controls.Add(this.widthLabel);
            this.sizeGroupBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sizeGroupBox.Location = new System.Drawing.Point(25, 27);
            this.sizeGroupBox.Margin = new System.Windows.Forms.Padding(16, 17, 0, 0);
            this.sizeGroupBox.Name = "sizeGroupBox";
            this.sizeGroupBox.Size = new System.Drawing.Size(260, 113);
            this.sizeGroupBox.TabIndex = 0;
            this.sizeGroupBox.TabStop = false;
            this.sizeGroupBox.Text = "Розмір вікна";
            // 
            // heightNumericUpDown
            // 
            this.heightNumericUpDown.DecimalPlaces = 2;
            this.heightNumericUpDown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.heightNumericUpDown.Location = new System.Drawing.Point(118, 65);
            this.heightNumericUpDown.Margin = new System.Windows.Forms.Padding(8, 9, 8, 0);
            this.heightNumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.heightNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.heightNumericUpDown.Name = "heightNumericUpDown";
            this.heightNumericUpDown.Size = new System.Drawing.Size(131, 26);
            this.heightNumericUpDown.TabIndex = 3;
            this.heightNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.heightLabel.Location = new System.Drawing.Point(7, 67);
            this.heightLabel.Margin = new System.Windows.Forms.Padding(4, 0, 3, 0);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(97, 18);
            this.heightLabel.TabIndex = 2;
            this.heightLabel.Text = "Висота (см):";
            // 
            // widthNumericUpDown
            // 
            this.widthNumericUpDown.DecimalPlaces = 2;
            this.widthNumericUpDown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.widthNumericUpDown.Location = new System.Drawing.Point(118, 28);
            this.widthNumericUpDown.Margin = new System.Windows.Forms.Padding(8, 4, 8, 0);
            this.widthNumericUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.widthNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.widthNumericUpDown.Name = "widthNumericUpDown";
            this.widthNumericUpDown.Size = new System.Drawing.Size(131, 26);
            this.widthNumericUpDown.TabIndex = 1;
            this.widthNumericUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.widthLabel.Location = new System.Drawing.Point(7, 30);
            this.widthLabel.Margin = new System.Windows.Forms.Padding(4, 0, 3, 0);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(100, 18);
            this.widthLabel.TabIndex = 0;
            this.widthLabel.Text = "Ширина (см):";
            // 
            // materialLabel
            // 
            this.materialLabel.AutoSize = true;
            this.materialLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialLabel.Location = new System.Drawing.Point(32, 155);
            this.materialLabel.Margin = new System.Windows.Forms.Padding(4, 0, 3, 0);
            this.materialLabel.Name = "materialLabel";
            this.materialLabel.Size = new System.Drawing.Size(80, 18);
            this.materialLabel.TabIndex = 7;
            this.materialLabel.Text = "Матеріал:";
            // 
            // materialСomboBox
            // 
            this.materialСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.materialСomboBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialСomboBox.FormattingEnabled = true;
            this.materialСomboBox.Items.AddRange(new object[] {
            "Дерево",
            "Метал",
            "Металопластик"});
            this.materialСomboBox.Location = new System.Drawing.Point(142, 152);
            this.materialСomboBox.Margin = new System.Windows.Forms.Padding(4, 9, 4, 4);
            this.materialСomboBox.Name = "materialСomboBox";
            this.materialСomboBox.Size = new System.Drawing.Size(132, 26);
            this.materialСomboBox.TabIndex = 6;
            // 
            // glazingGroupBox
            // 
            this.glazingGroupBox.AutoSize = true;
            this.glazingGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.glazingGroupBox.Controls.Add(this.doubleGlazingRadioButton);
            this.glazingGroupBox.Controls.Add(this.singleGlazingRadioButton);
            this.glazingGroupBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.glazingGroupBox.Location = new System.Drawing.Point(309, 27);
            this.glazingGroupBox.Margin = new System.Windows.Forms.Padding(32, 17, 16, 0);
            this.glazingGroupBox.Name = "glazingGroupBox";
            this.glazingGroupBox.Size = new System.Drawing.Size(151, 113);
            this.glazingGroupBox.TabIndex = 8;
            this.glazingGroupBox.TabStop = false;
            this.glazingGroupBox.Text = "Склопакет";
            // 
            // doubleGlazingRadioButton
            // 
            this.doubleGlazingRadioButton.AutoSize = true;
            this.doubleGlazingRadioButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.doubleGlazingRadioButton.Location = new System.Drawing.Point(11, 65);
            this.doubleGlazingRadioButton.Margin = new System.Windows.Forms.Padding(8, 9, 4, 0);
            this.doubleGlazingRadioButton.Name = "doubleGlazingRadioButton";
            this.doubleGlazingRadioButton.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.doubleGlazingRadioButton.Size = new System.Drawing.Size(124, 26);
            this.doubleGlazingRadioButton.TabIndex = 1;
            this.doubleGlazingRadioButton.Text = "Двокамерний";
            this.doubleGlazingRadioButton.UseVisualStyleBackColor = true;
            // 
            // singleGlazingRadioButton
            // 
            this.singleGlazingRadioButton.AutoSize = true;
            this.singleGlazingRadioButton.Checked = true;
            this.singleGlazingRadioButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.singleGlazingRadioButton.Location = new System.Drawing.Point(11, 28);
            this.singleGlazingRadioButton.Margin = new System.Windows.Forms.Padding(8, 4, 4, 0);
            this.singleGlazingRadioButton.Name = "singleGlazingRadioButton";
            this.singleGlazingRadioButton.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.singleGlazingRadioButton.Size = new System.Drawing.Size(133, 26);
            this.singleGlazingRadioButton.TabIndex = 0;
            this.singleGlazingRadioButton.TabStop = true;
            this.singleGlazingRadioButton.Text = "Однокамерний";
            this.singleGlazingRadioButton.UseVisualStyleBackColor = true;
            // 
            // windowsillCheckBox
            // 
            this.windowsillCheckBox.AutoSize = true;
            this.windowsillCheckBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.windowsillCheckBox.Location = new System.Drawing.Point(320, 152);
            this.windowsillCheckBox.Margin = new System.Windows.Forms.Padding(0, 9, 0, 0);
            this.windowsillCheckBox.Name = "windowsillCheckBox";
            this.windowsillCheckBox.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.windowsillCheckBox.Size = new System.Drawing.Size(105, 26);
            this.windowsillCheckBox.TabIndex = 9;
            this.windowsillCheckBox.Text = "Підвіконня";
            this.windowsillCheckBox.UseVisualStyleBackColor = true;
            // 
            // resultLabel
            // 
            this.resultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultLabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultLabel.ForeColor = System.Drawing.Color.LimeGreen;
            this.resultLabel.Location = new System.Drawing.Point(25, 219);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(16, 34, 16, 34);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(277, 25);
            this.resultLabel.TabIndex = 10;
            this.resultLabel.Visible = false;
            // 
            // calculateButton
            // 
            this.calculateButton.AutoSize = true;
            this.calculateButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calculateButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateButton.Location = new System.Drawing.Point(318, 214);
            this.calculateButton.Margin = new System.Windows.Forms.Padding(0, 34, 16, 0);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Padding = new System.Windows.Forms.Padding(16, 2, 16, 2);
            this.calculateButton.Size = new System.Drawing.Size(141, 32);
            this.calculateButton.TabIndex = 11;
            this.calculateButton.Text = "Розрахувати";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButtonClick);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(284, 266);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // GlazedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 297);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.windowsillCheckBox);
            this.Controls.Add(this.glazingGroupBox);
            this.Controls.Add(this.materialLabel);
            this.Controls.Add(this.materialСomboBox);
            this.Controls.Add(this.sizeGroupBox);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(500, 336);
            this.MinimumSize = new System.Drawing.Size(500, 336);
            this.Name = "GlazedForm";
            this.Text = "Лабораторна робота No6. Завдання 2";
            this.Load += new System.EventHandler(this.GlazedForm_Load);
            this.sizeGroupBox.ResumeLayout(false);
            this.sizeGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heightNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.widthNumericUpDown)).EndInit();
            this.glazingGroupBox.ResumeLayout(false);
            this.glazingGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox sizeGroupBox;
        private System.Windows.Forms.NumericUpDown heightNumericUpDown;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.NumericUpDown widthNumericUpDown;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label materialLabel;
        private System.Windows.Forms.ComboBox materialСomboBox;
        private System.Windows.Forms.GroupBox glazingGroupBox;
        private System.Windows.Forms.RadioButton doubleGlazingRadioButton;
        private System.Windows.Forms.RadioButton singleGlazingRadioButton;
        private System.Windows.Forms.CheckBox windowsillCheckBox;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label label3;
    }
}

