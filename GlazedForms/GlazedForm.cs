﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms {

    public partial class GlazedForm : Form {

        public GlazedForm() {
            InitializeComponent();
        }

        private void GlazedForm_Load(object sender, EventArgs e) {
            materialСomboBox.SelectedIndex = 0;
        }

        private void calculateButtonClick(object sender, EventArgs e) {
            double cost = doubleGlazingRadioButton.Checked ? 0.1 : 0.05;
            switch (materialСomboBox.SelectedIndex) {
                case 0:
                    cost += 0.2;
                    break;
                case 1:
                    cost += 0;
                    break;
                case 2:
                    cost += 0.1;
                    break;
                default:
                    showError("Оберіть матеріал!!!");
                    return;
            }

            double width = (double) widthNumericUpDown.Value;
            if (width < 1) {
                showError("Ширина вікна не може бути менше 1 см.");
                return;
            }

            double height = (double) heightNumericUpDown.Value;
            if (width < 1) {
                showError("Висота вікна не може бути менше 1 см.");
                return;
            }

            double result = (width * height) * cost;
            if (windowsillCheckBox.Checked) result += 35;

            resultLabel.Visible = true;
            resultLabel.Text = $"Вартість: {result:F2} грн.";

        }

        private void showError(string message) {
            MessageBox.Show(message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
