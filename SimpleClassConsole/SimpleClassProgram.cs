﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleClassLibrary;

namespace SimpleClassConsole {
    internal class SimpleClassProgram {

        private const ConsoleColor defTextColor = ConsoleColor.White;
        private const ConsoleColor errorColor = ConsoleColor.Red;
        private const ConsoleColor titleColor = ConsoleColor.Green;
        private const ConsoleColor hintColor = ConsoleColor.Yellow;

        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Title = "Лабораторна робота No6. Завдання 1";
            Console.ForegroundColor = defTextColor;
            start(new Airplane[0]);
        }

        private static void start(Airplane[] items) {
            printMenuTitle("Меню");
            int select = selectMenuItem(new string[] {
                "Вихід",
                "створити рейси",
                "показати список рейсів",
                "показати рейс за номером",
                "показати рейси з відправленням і прибуттям в один день",
                "показати найбільший та найменший час подорожі",
                "відсортувати за спаданням дати відправлення",
                "відсортувати за зростанням часу подорожі",
            });

            Console.Clear();
            switch (select) {
                case 0:
                    return;
                case 1:
                    printMenuTitle("створити рейси");
                    items = ReadAirplaneArray();
                    Console.Clear();
                    break;
                case 2:
                    printMenuTitle("список рейсів");
                    PrintAirplanes(items);
                    break;
                case 3:
                    printMenuTitle("рейс за номером");
                    if (items == null || items.Length < 1) writeLine("Список рейсів порожній!!!", hintColor);
                    else PrintAirplane(items, inputInt($"номер студента від 1 до {items.Length}", 1, items.Length));
                    break;
                case 4:
                    printMenuTitle("список рейсів з відправленням і прибуттям в один день");
                    PrintAirplanes(items, true);
                    break;
                case 5:
                    printMenuTitle("найбільший та найменший час подорожі");
                    if (items == null || items.Length < 1) {
                        writeLine("Список рейсів порожній!!!", hintColor);
                    } else {
                        long max;
                        long min;
                        GetAirplaneInfo(items, out min, out max);
                        write("найбільший час подорожі : ");
                        writeLine($"{max} хвилин", hintColor);
                        write("найменший час подорожі : ");
                        writeLine($"{min} хвилин", hintColor);
                    }
                    break;
                case 6:
                    printMenuTitle("відсортувати за спаданням дати відправлення");
                    SortAirplanesByDate(items);
                    break;
                case 7:
                    printMenuTitle("відсортувати за зростанням часу подорожі");
                    SortAirplanesByTotalTime(items);
                    break;
                default:
                    break;
            }

            start(items);
        }

        private static Airplane[] ReadAirplaneArray() {
            int count = inputInt("скільки рейсів ви хочете створити", 1, int.MaxValue);
            Airplane[] items = new Airplane[count];

            for (int i = 0; i < count; i++) {
                printMenuTitle($"рейс No{i + 1}");
                string startCity = inpuString("місто відправлення");
                string finishCity = inpuString("місто прибуття");
                Date startDate = ReadDate("відправлення");
                Date finishDate = ReadDate("прибуття", startDate);
                items[i] = new Airplane(startCity, finishCity, startDate, finishDate, ReadFlightRange());
            }

            return items;
        }

        private static Date ReadDate(string typeName, Date min = null) {
            printTitle($"дата {typeName}", 40, hintColor);
            Date it = new Date(
                inputInt("рік у форматі \"yyyy\"", min: 0),
                inputInt("місяц у форматі \"MM\"", 1, 12),
                inputInt("день у форматі \"dd\"", 1, 30),
                inputInt("години у форматі \"HH\"", 0, 23),
                inputInt("хвилини у форматі \"mm\"", 0, 59)
            );
            printTitle("", 40, hintColor);

            if (min == null || it.GetTimeInMinutes() > min.GetTimeInMinutes()) return it;

            writeLine($"дата {typeName} повинна бути більшою за {min.ToString()}", errorColor);
            return ReadDate(typeName, min);
        }

        private static FlightRange ReadFlightRange() {
            printTitle($"дільність польту", 40, hintColor);
            writeLine("Оберіть одиниці вимірювання дальності польоту");
            string[] items = new string[] { "в кілометрах", "в метрах", "в милях" };
            int select = selectMenuItem(items, 1);
            FlightRange.UnitsMeasurement unit = FlightRange.UnitsMeasurement.M;
            switch (select) {
                case 0:
                    unit = FlightRange.UnitsMeasurement.KM;
                    break;
                case 1:
                    unit = FlightRange.UnitsMeasurement.M;
                    break;
                case 2:
                    unit = FlightRange.UnitsMeasurement.MI;
                    break;
            }
            return new FlightRange(inputDouble($"відстань {items[select]}", min: 0), unit);
        }

        private static void PrintAirplanes(Airplane[] items, bool isShowArrivingToday = false) {
            if (items == null || items.Length < 1) {
                writeLine("Список рейсів порожній!!!", hintColor);
                return;
            }

            for (int i = 0; i < items.Length; i++) {
                if (isShowArrivingToday && !items[i].IsArrivingToday()) continue;
                PrintAirplane(items, i + 1);
            }
        }

        private static void PrintAirplane(Airplane[] items, int number) {
            if (items == null || items.Length < 1 || number < 1 || number > items.Length) return;

            Airplane it = items[number - 1];
            printTitle($"рейс No{number}", 40, hintColor);
            PrintAirplane(it);
            printTitle("", 40, hintColor);
        }

        private static void PrintAirplane(Airplane item) {
            writeLine(item.ToString());
        }

        private static void GetAirplaneInfo(Airplane[] items, out long minTotalTime, out long maxTotalTime) {
            long min = long.MaxValue, max = 0;

            foreach (Airplane it in items) {
                long totalTime = it.GetTotalTime();
                if (totalTime < 0) continue;
                if (totalTime > max) max = totalTime;
                if (totalTime < min) min = totalTime;
            }

            minTotalTime = min;
            maxTotalTime = max;
        }

        private static void SortAirplanesByDate(Airplane[] items) {
            Array.Sort(items, SortAirplanesByDate);
            PrintAirplanes(items);
        }

        private static int SortAirplanesByDate(Airplane first, Airplane second) {
            long a = first.StartDate.GetTimeInMinutes(), b = second.StartDate.GetTimeInMinutes();
            return a > b ? -1 : a < b ? 1 : 0;
        }

        private static void SortAirplanesByTotalTime(Airplane[] items) {
            Array.Sort(items, SortAirplanesByTotalTime);
            PrintAirplanes(items);
        }

        private static int SortAirplanesByTotalTime(Airplane first, Airplane second) {
            long a = first.GetTotalTime(), b = second.GetTotalTime();
            return a > b ? 1 : a < b ? -1 : 0;
        }

        private static int inputInt(string name, int min = int.MinValue, int max = int.MaxValue) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
                return inputInt(name, min, max);
            }

            try {
                int n = int.Parse(value);

                if (n < min) writeLine($"\nЗначення не може бути меншим за {min}. Будь ласка, повторіть спробу!!!", errorColor);
                else if (n > max) writeLine($"\nЗначення не може бути білшим за {max}. Будь ласка, повторіть спробу!!!", errorColor);
                else return n;

                return inputInt(name, min, max);
            } catch (Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть спробу!!!",
                    errorColor
                );
                return inputInt(name, min, max);
            }
        }
        private static double inputDouble(string name, double min = double.MinValue, double max = double.MaxValue) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
                return inputDouble(name, min, max);
            }

            try {
                double n = double.Parse(value);

                if (n < min) writeLine($"\nЗначення не може бути меншим за {min}. Будь ласка, повторіть спробу!!!", errorColor);
                else if (n > max) writeLine($"\nЗначення не може бути білшим за {max}. Будь ласка, повторіть спробу!!!", errorColor);
                else return n;

                return inputDouble(name, min, max);
            } catch (Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть спробу!!!",
                    errorColor
                );
                return inputDouble(name, min, max);
            }
        }

        private static string inpuString(string name) {
            writeLine($"Вкажіть {name} та натисніть Enter");
            string value = Console.ReadLine();
            if (!string.IsNullOrEmpty(value)) return value;

            writeLine("Значення не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
            return inpuString(name);
        }

        private static int selectMenuItem(string[] items, int def = -1) {
            if (items == null || items.Length == 0) return def;

            writeLine("\nВкажіть номер пункту та натисніть Enter");

            for (int i = 1; i < items.Length; i++) printMenuItem(i, items[i]);
            printMenuItem(0, items[0]);
            try {
                int it = int.Parse(Console.ReadLine());
                if (it >= 0 && it < items.Length) return it;
                throw new System.FormatException();
            } catch (Exception) {
                writeLine("\nЗробіть свій вибір!!!", errorColor);
            }

            return selectMenuItem(items, def);
        }

        private static void printMenuItem(int number, string name) {
            write($"\t{number}", hintColor);
            writeLine($" -> {name}.");
        }

        private static void printMenuTitle(string name) {
            printTitle(name, 120, titleColor);
        }

        private static void printTitle(string name, int spaseLenght, ConsoleColor color = defTextColor) {
            string it = $"----------{name}";
            for (int i = it.Length; i < spaseLenght; i++) it += "-";
            writeLine();
            writeLine(it, color);
            writeLine();
        }

        private static void writeLine(string value = "", ConsoleColor color = defTextColor) {
            write($"{value}\n", color);
        }

        private static void write(string value = "", ConsoleColor color = defTextColor) {
            Console.ForegroundColor = color;
            Console.Write(value);
            Console.ForegroundColor = defTextColor;
        }
    }
}
