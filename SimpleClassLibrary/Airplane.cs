﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SimpleClassLibrary.FlightRange;

namespace SimpleClassLibrary {

    public class Airplane {

        private FlightRange distance = new FlightRange();

        public string StartCity { get; set; } = "";
        public string FinishCity { get; set; } = "";
        public Date StartDate { get; set; } = null;
        public Date FinishDate { get; set; } = null;

        private Airplane() { }

        public Airplane(string startCity, string finishCity, Date startDate, Date finishDate, FlightRange distance = null) {
            StartCity = startCity;
            FinishCity = finishCity;
            StartDate = startDate;
            FinishDate = finishDate;
            if (distance != null) this.distance = distance;
        }

        public Airplane(Airplane it, string startCity = null, string finishCity = null, Date startDate = null, Date finishDate = null, FlightRange distance = null) {
            StartCity = startCity == null ? it.StartCity : startCity;
            FinishCity = finishCity == null ? it.FinishCity : finishCity;
            StartDate = startDate == null ? it.StartDate : startDate;
            FinishDate = finishDate == null ? it.FinishDate : finishDate;
            this.distance = distance == null ? it.distance : distance;
        }

        public void SetDistance(double distance, FlightRange.UnitsMeasurement unit = FlightRange.UnitsMeasurement.M) {
            this.distance.SetDistance(distance, unit);
        }

        private double GetDistance(UnitsMeasurement unit = UnitsMeasurement.M) {
            return this.distance.GetDistance(unit);
        }

        public override string ToString() {
            string it = $"місто відправлення : {(string.IsNullOrEmpty(StartCity) ? "невідомо" : StartCity)}\n";
            it += $"місто прибуття : {(string.IsNullOrEmpty(FinishCity) ? "невідомо" : FinishCity)}\n";
            it += $"дата відправлення : {(StartDate == null ? "невідомо" : StartDate.ToString())}\n";
            it += $"дата прибуття : {(FinishDate == null ? "невідомо" : FinishDate.ToString())}\n";
            it += distance.ToString();
            return it;
        }

        public long GetTotalTime() {
            return (StartDate == null || FinishDate == null) ? -1 : FinishDate.GetTimeInMinutes() - StartDate.GetTimeInMinutes();
        }

        public bool IsArrivingToday() {
            return StartDate != null && FinishDate != null && StartDate.IsToday(FinishDate);
        }
    }
}
