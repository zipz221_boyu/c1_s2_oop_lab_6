﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary {

    public class Date {

        private int year = -1;
        private int month = 0;
        private int day = 0;
        private int hours = -1;
        private int minutes = -1;

        public int Year {
            get => year;
            set => year = value < 0 ? 0 : value;
        }
        public int Month {
            get => month;
            set => month = value < 1 || value > 12 ? 1 : value;
        }
        public int Day {
            get => day;
            set => day = value < 1 || value > 30 ? 1 : value;
        }
        public int Hours {
            get => hours;
            set => hours = value < 0 || value > 23 ? 0 : value;
        }
        public int Minutes {
            get => minutes;
            set => minutes = value < 0 || value > 59 ? 0 : value;
        }

        private Date() { }

        public Date(int year = 0, int month = 1, int day = 1, int hours = 0, int minute = 0) {
            Year = year;
            Month = month;
            Day = day;
            Hours = hours;
            Minutes = minute;
        }

        public Date(Date it, int year = -1, int month = -1, int day = -1, int hours = -1, int minute = -1) {
            Year = year < 0 ? it.Year : year;
            Month = month < 0 ? it.Month : month;
            Day = day < 0 ? it.Day : day;
            Hours = hours < 0 ? it.Hours : hours;
            Minutes = minute < 0 ? it.Minutes : minute;
        }

        public override string ToString() {
            return $"{day:D2}.{month:D2}.{year} {hours:D2}:{minutes:D2}";
        }

        public long GetTimeInMinutes() {
            long time = GetYearInMinutes(year);
            time += GetMonthInMinutes(month);
            time += GetDayInMinutes(day);
            time += GetHoursInMinutes(hours);
            time += minutes;
            return time;
        }

        public bool IsToday(Date date) {
            return date != null && year == date.Year && month == date.Month && day == date.Day;
        }

        private long GetYearInMinutes(int year) {
            return GetDayInMinutes(year * 365);
        }

        private long GetMonthInMinutes(int month) {
            return GetDayInMinutes(month * 30);
        }

        private long GetDayInMinutes(int day) {
            return GetHoursInMinutes(day * 24);
        }

        private long GetHoursInMinutes(int hour) {
            return hour * 60;
        }
    }
}
