﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleClassLibrary {
    public class FlightRange {

        private double distanceInMeters = 0;

        public FlightRange() { }

        public FlightRange(double distance, UnitsMeasurement unit = UnitsMeasurement.M) {
            SetDistance(distance, unit);
        }

        public void SetDistance(double value, UnitsMeasurement unit = UnitsMeasurement.M) {
            if (value < 0) return;
            distanceInMeters = Math.Round(value * GetRatioToMeter(unit), 2);
        }

        public double GetDistance(UnitsMeasurement unit = UnitsMeasurement.M) {
            return Math.Round(distanceInMeters / GetRatioToMeter(unit), 2);
        }

        public string ToString(UnitsMeasurement unit) {
            string it = "Дальність польоту в ";
            switch (unit) {
                case UnitsMeasurement.KM:
                    it += "кілометрах";
                    break;
                case UnitsMeasurement.M:
                    it += "метрах";
                    break;
                case UnitsMeasurement.MI:
                    it += "милях";
                    break;
            }
            return it + $" : {GetDistance(unit)}";
        }

        public override string ToString() {
            return $"{ToString(UnitsMeasurement.KM)}\n{ToString(UnitsMeasurement.M)}\n{ToString(UnitsMeasurement.MI)}\n";
        }

        private double GetRatioToMeter(UnitsMeasurement unit) {
            switch (unit) {
                case UnitsMeasurement.KM: return 1000;
                case UnitsMeasurement.M: return 1;
                case UnitsMeasurement.MI: return 1609.344;
                default: return 1;
            }
        }


        public enum UnitsMeasurement {
            KM, M, MI
        }

    }
}
