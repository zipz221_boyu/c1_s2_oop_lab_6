﻿namespace TravelForms
{
    partial class TravelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tourGroupBox = new System.Windows.Forms.GroupBox();
            this.durationNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.durationLabel = new System.Windows.Forms.Label();
            this.countryLabel = new System.Windows.Forms.Label();
            this.countryСomboBox = new System.Windows.Forms.ComboBox();
            this.seasonGroupBox = new System.Windows.Forms.GroupBox();
            this.winterSeasonRadioButton = new System.Windows.Forms.RadioButton();
            this.summerSeasonRadioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.calculateButton = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            this.guideCheckBox = new System.Windows.Forms.CheckBox();
            this.tourGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.durationNumericUpDown)).BeginInit();
            this.seasonGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tourGroupBox
            // 
            this.tourGroupBox.AutoSize = true;
            this.tourGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tourGroupBox.Controls.Add(this.durationNumericUpDown);
            this.tourGroupBox.Controls.Add(this.durationLabel);
            this.tourGroupBox.Controls.Add(this.countryLabel);
            this.tourGroupBox.Controls.Add(this.countryСomboBox);
            this.tourGroupBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tourGroupBox.Location = new System.Drawing.Point(25, 26);
            this.tourGroupBox.Margin = new System.Windows.Forms.Padding(16, 17, 0, 0);
            this.tourGroupBox.Name = "tourGroupBox";
            this.tourGroupBox.Size = new System.Drawing.Size(300, 113);
            this.tourGroupBox.TabIndex = 14;
            this.tourGroupBox.TabStop = false;
            this.tourGroupBox.Text = "Тур";
            // 
            // durationNumericUpDown
            // 
            this.durationNumericUpDown.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.durationNumericUpDown.Location = new System.Drawing.Point(158, 65);
            this.durationNumericUpDown.Margin = new System.Windows.Forms.Padding(8, 9, 8, 0);
            this.durationNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.durationNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.durationNumericUpDown.Name = "durationNumericUpDown";
            this.durationNumericUpDown.Size = new System.Drawing.Size(131, 26);
            this.durationNumericUpDown.TabIndex = 3;
            this.durationNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // durationLabel
            // 
            this.durationLabel.AutoSize = true;
            this.durationLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.durationLabel.Location = new System.Drawing.Point(7, 67);
            this.durationLabel.Margin = new System.Windows.Forms.Padding(4, 0, 3, 0);
            this.durationLabel.Name = "durationLabel";
            this.durationLabel.Size = new System.Drawing.Size(140, 18);
            this.durationLabel.TabIndex = 2;
            this.durationLabel.Text = "Тривалість (день):";
            // 
            // countryLabel
            // 
            this.countryLabel.AutoSize = true;
            this.countryLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countryLabel.Location = new System.Drawing.Point(7, 32);
            this.countryLabel.Margin = new System.Windows.Forms.Padding(4, 0, 3, 0);
            this.countryLabel.Name = "countryLabel";
            this.countryLabel.Size = new System.Drawing.Size(59, 18);
            this.countryLabel.TabIndex = 16;
            this.countryLabel.Text = "Країна:";
            // 
            // countryСomboBox
            // 
            this.countryСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.countryСomboBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countryСomboBox.FormattingEnabled = true;
            this.countryСomboBox.Items.AddRange(new object[] {
            "Болгарія",
            "Німеччина",
            "Польща"});
            this.countryСomboBox.Location = new System.Drawing.Point(157, 26);
            this.countryСomboBox.Margin = new System.Windows.Forms.Padding(4, 9, 8, 4);
            this.countryСomboBox.Name = "countryСomboBox";
            this.countryСomboBox.Size = new System.Drawing.Size(132, 26);
            this.countryСomboBox.TabIndex = 15;
            // 
            // seasonGroupBox
            // 
            this.seasonGroupBox.AutoSize = true;
            this.seasonGroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.seasonGroupBox.Controls.Add(this.winterSeasonRadioButton);
            this.seasonGroupBox.Controls.Add(this.summerSeasonRadioButton);
            this.seasonGroupBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.seasonGroupBox.Location = new System.Drawing.Point(357, 26);
            this.seasonGroupBox.Margin = new System.Windows.Forms.Padding(32, 17, 16, 0);
            this.seasonGroupBox.Name = "seasonGroupBox";
            this.seasonGroupBox.Size = new System.Drawing.Size(82, 113);
            this.seasonGroupBox.TabIndex = 17;
            this.seasonGroupBox.TabStop = false;
            this.seasonGroupBox.Text = "Сезон";
            // 
            // winterSeasonRadioButton
            // 
            this.winterSeasonRadioButton.AutoSize = true;
            this.winterSeasonRadioButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.winterSeasonRadioButton.Location = new System.Drawing.Point(11, 65);
            this.winterSeasonRadioButton.Margin = new System.Windows.Forms.Padding(8, 9, 4, 0);
            this.winterSeasonRadioButton.Name = "winterSeasonRadioButton";
            this.winterSeasonRadioButton.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.winterSeasonRadioButton.Size = new System.Drawing.Size(64, 26);
            this.winterSeasonRadioButton.TabIndex = 1;
            this.winterSeasonRadioButton.Text = "Зима";
            this.winterSeasonRadioButton.UseVisualStyleBackColor = true;
            // 
            // summerSeasonRadioButton
            // 
            this.summerSeasonRadioButton.AutoSize = true;
            this.summerSeasonRadioButton.Checked = true;
            this.summerSeasonRadioButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.summerSeasonRadioButton.Location = new System.Drawing.Point(11, 28);
            this.summerSeasonRadioButton.Margin = new System.Windows.Forms.Padding(8, 4, 4, 0);
            this.summerSeasonRadioButton.Name = "summerSeasonRadioButton";
            this.summerSeasonRadioButton.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.summerSeasonRadioButton.Size = new System.Drawing.Size(57, 26);
            this.summerSeasonRadioButton.TabIndex = 0;
            this.summerSeasonRadioButton.TabStop = true;
            this.summerSeasonRadioButton.Text = "Літо";
            this.summerSeasonRadioButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(263, 268);
            this.label3.Margin = new System.Windows.Forms.Padding(0, 0, 0, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(198, 18);
            this.label3.TabIndex = 21;
            this.label3.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // calculateButton
            // 
            this.calculateButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.calculateButton.AutoSize = true;
            this.calculateButton.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.calculateButton.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.calculateButton.Location = new System.Drawing.Point(298, 214);
            this.calculateButton.Margin = new System.Windows.Forms.Padding(0, 34, 16, 0);
            this.calculateButton.Name = "calculateButton";
            this.calculateButton.Padding = new System.Windows.Forms.Padding(16, 2, 16, 2);
            this.calculateButton.Size = new System.Drawing.Size(141, 32);
            this.calculateButton.TabIndex = 20;
            this.calculateButton.Text = "Розрахувати";
            this.calculateButton.UseVisualStyleBackColor = true;
            this.calculateButton.Click += new System.EventHandler(this.calculateButtonClick);
            // 
            // resultLabel
            // 
            this.resultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultLabel.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultLabel.ForeColor = System.Drawing.Color.LimeGreen;
            this.resultLabel.Location = new System.Drawing.Point(25, 218);
            this.resultLabel.Margin = new System.Windows.Forms.Padding(16, 34, 16, 34);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(257, 25);
            this.resultLabel.TabIndex = 19;
            this.resultLabel.Visible = false;
            // 
            // guideCheckBox
            // 
            this.guideCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guideCheckBox.AutoSize = true;
            this.guideCheckBox.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guideCheckBox.Location = new System.Drawing.Point(35, 147);
            this.guideCheckBox.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.guideCheckBox.Name = "guideCheckBox";
            this.guideCheckBox.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.guideCheckBox.Size = new System.Drawing.Size(157, 26);
            this.guideCheckBox.TabIndex = 18;
            this.guideCheckBox.Text = "Індивідуальний гід";
            this.guideCheckBox.UseVisualStyleBackColor = true;
            // 
            // TravelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 297);
            this.Controls.Add(this.tourGroupBox);
            this.Controls.Add(this.seasonGroupBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.calculateButton);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.guideCheckBox);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(480, 336);
            this.MinimumSize = new System.Drawing.Size(480, 336);
            this.Name = "TravelForm";
            this.Text = "Лабораторна робота No6. Завдання 3";
            this.Load += new System.EventHandler(this.TravelForm_Load);
            this.tourGroupBox.ResumeLayout(false);
            this.tourGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.durationNumericUpDown)).EndInit();
            this.seasonGroupBox.ResumeLayout(false);
            this.seasonGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox tourGroupBox;
        private System.Windows.Forms.NumericUpDown durationNumericUpDown;
        private System.Windows.Forms.Label durationLabel;
        private System.Windows.Forms.GroupBox seasonGroupBox;
        private System.Windows.Forms.RadioButton winterSeasonRadioButton;
        private System.Windows.Forms.RadioButton summerSeasonRadioButton;
        private System.Windows.Forms.Label countryLabel;
        private System.Windows.Forms.ComboBox countryСomboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button calculateButton;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.CheckBox guideCheckBox;
    }
}

