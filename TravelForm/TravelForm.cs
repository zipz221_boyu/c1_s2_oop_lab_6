﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelForms {

    public partial class TravelForm : Form {
        public TravelForm() {
            InitializeComponent();
        }

        private void TravelForm_Load(object sender, EventArgs e) {
            countryСomboBox.SelectedIndex = 0;
        }

        private void calculateButtonClick(object sender, EventArgs e) {
            int cost = 0;
            int seasonCost = 0;

            switch (countryСomboBox.SelectedIndex) {
                case 0:
                    cost = 100;
                    seasonCost = 50;
                    break;
                case 1:
                    cost = 160;
                    seasonCost = 40;
                    break;
                case 2:
                    cost = 120;
                    seasonCost = 60;
                    break;
                default:
                    showError("Оберіть країну!!!");
                    return;
            }

            if (winterSeasonRadioButton.Checked) cost += seasonCost;

            int duration = (int) durationNumericUpDown.Value;
            if (duration < 1) {
                showError("Тривалість туру не може бути менше 1 дня.");
                return;
            }

            int result = duration * cost;
            if (guideCheckBox.Checked) result += 50 * duration;

            resultLabel.Visible = true;
            resultLabel.Text = $"Вартість: $ {result}";
        }

        private void showError(string message) {
            MessageBox.Show(message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
